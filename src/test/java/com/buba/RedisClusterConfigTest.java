package com.buba;


import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import redis.clients.jedis.JedisCluster;

@SpringBootTest
public class RedisClusterConfigTest {
    @Autowired
    private JedisCluster jedisCluster;

    @Test
    public void test() {
        jedisCluster.set("age", "19");
        String age = jedisCluster.get("age");
        System.out.println("age======" + age);
    }
}
