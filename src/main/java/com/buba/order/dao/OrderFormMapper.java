package com.buba.order.dao;

import com.buba.order.pojo.OrderForm;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
@Mapper
public interface OrderFormMapper {
    Integer addOrderForm(Map<String, Object> map);

    List<OrderForm> selOrderForm(Map<String, Object> parameterMap);

    Long selOrderFormCount(Map<String, Object> parameterMap);

    Long updOrderForm(Map<String, Object> map);

    Integer delOrderFormAll(List<String> list);

    List<OrderForm> selOrderFormByCondition(Map<String, Object> map);

    Long selOrderFormCountForTreasurer(Map<String, Object> parameterMap);

    List<OrderForm> selOrderFormForTreasurer(Map<String, Object> parameterMap);

    Integer batchPass(List<String> list);

    Integer batchNoPass(List<String> list);

    Long pass(Map<String, Object> map);

    Long noPass(Map<String, Object> map);
}
