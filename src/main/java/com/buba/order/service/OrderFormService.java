package com.buba.order.service;


import com.buba.order.pojo.OrderForm;

import java.util.List;
import java.util.Map;

public interface OrderFormService {
    Integer addOrderForm(Map<String, Object> map);

    List<OrderForm> selOrderForm(Map<String, Object> parameterMap);

    Long selOrderFormCount(Map<String, Object> parameterMap);

    Long updOrderForm(Map<String, Object> map);

    Integer delOrderFormAll(List<String> list);

    List<OrderForm> selOrderFormByCondition(Map<String, Object> map);

    Long selOrderFormCountForTreasurer(Map<String, Object> parameterMap);

    List<OrderForm> selOrderFormForTreasurer(Map<String, Object> parameterMap);

    Integer batchPass(List<String> list);

    Integer batchNoPass(List<String> list);

    Long pass(Map<String, Object> map);

    Long noPass(Map<String, Object> map);
}
