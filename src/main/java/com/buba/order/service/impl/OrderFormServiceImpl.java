package com.buba.order.service.impl;

import com.buba.order.dao.OrderFormMapper;
import com.buba.order.pojo.OrderForm;
import com.buba.order.service.OrderFormService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class OrderFormServiceImpl implements OrderFormService {
    private OrderFormMapper orderFormMapper;

    @Override
    public Integer addOrderForm(Map<String, Object> map) {
        return orderFormMapper.addOrderForm(map);
    }

    @Override
    public List<OrderForm> selOrderForm(Map<String, Object> parameterMap) {
        return orderFormMapper.selOrderForm(parameterMap);
    }

    @Override
    public Long selOrderFormCount(Map<String, Object> parameterMap) {
        return orderFormMapper.selOrderFormCount(parameterMap);
    }

    @Override
    public Long updOrderForm(Map<String, Object> map) {
        return orderFormMapper.updOrderForm(map);
    }

    @Override
    public Integer delOrderFormAll(List<String> list) {
        return orderFormMapper.delOrderFormAll(list);
    }

    @Override
    public List<OrderForm> selOrderFormByCondition(Map<String, Object> map) {
        return orderFormMapper.selOrderFormByCondition(map);
    }

    @Override
    public Long selOrderFormCountForTreasurer(Map<String, Object> parameterMap) {
        return orderFormMapper.selOrderFormCountForTreasurer(parameterMap);
    }

    @Override
    public List<OrderForm> selOrderFormForTreasurer(Map<String, Object> parameterMap) {
        return orderFormMapper.selOrderFormForTreasurer(parameterMap);
    }

    @Override
    public Integer batchPass(List<String> list) {
        return orderFormMapper.batchPass(list);
    }

    @Override
    public Integer batchNoPass(List<String> list) {
        return orderFormMapper.batchNoPass(list);
    }

    @Override
    public Long pass(Map<String, Object> map) {
        return orderFormMapper.pass(map);
    }

    @Override
    public Long noPass(Map<String, Object> map) {
        return orderFormMapper.noPass(map);
    }


    @Autowired
    public void setOrderFormMapper(OrderFormMapper orderFormMapper) {
        this.orderFormMapper = orderFormMapper;
    }
}
