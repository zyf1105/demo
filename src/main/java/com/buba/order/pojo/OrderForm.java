package com.buba.order.pojo;

import com.buba.goods.annotation.ExcelCell;
import lombok.Data;

import java.math.BigDecimal;

/**
 * 欲售货订单/售货订单/未通过的售货订单
 */
@Data
public class OrderForm {
   private String xHOrderNo;//售货订单编号
   private String xHOrderDate;//售货订单日期
   private String gDDate;//取货日期
   private String gDAddress;//取货地点
   private String revCurrencyModeNo;//收款方式编号
   private String revCurrencyLastDate;//收款最迟日期
   private String currencyNo;//货币种类编号
   private Double prePayCurrency;//预付金额
   private Double goodsSumCurrency;//货物总额
   private Double taxSumCurrency;//税率总额
   private Double creditCurrency;//赊欠金额
   private Double addtionalCurrency;//运调费用
   private String traderNo;//业务员编号
   private String treasurerNo;//财务员编号
   private Integer isCheck;//审核
   private String checkRes;//审核结果
   private Integer recCurrencyRcept;//收款收据
   private String customerNo;//客户信息编号
   private Integer goodsClassCounts;//货物种类数
   private Double sumProfitCurrency;//总净高额
   private Integer itemsQty;//订货数量
   private BigDecimal itemsPrice;//订货单价
   private String itemsInfoNo;//物质明细表编号
   private String itemsNo;//物质价格表编号
   private Double itemsProfitPrice;//物质净高价（订货单价 - 物质价格）
}
