package com.buba.order.controller;

import com.buba.goods.service.MatterPriceService;
import com.buba.order.pojo.OrderForm;
import com.buba.order.service.OrderFormService;
import com.buba.system.pojo.Employee;
import com.buba.system.pojo.Job;
import com.buba.util.GenerateKey;
import com.buba.util.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 欲售货订单/售货订单/未通过的售货订单
 */
@RestController
@RequestMapping("/OrderForm")
public class OrderFormController {
    private OrderFormService orderFormService;
    private MatterPriceService matterPriceService;


    /**
     * 鉴别登录用户是否拥有业务员权限
     * @param session
     * @throws Exception
     */
    @RequestMapping("/identificationSalesman")
    public Map<String,Object> identificationSalesman(HttpSession session) throws Exception {
        Map<String,Object> map = new HashMap<>();
        map.put("message","");
        Employee employee = (Employee)session.getAttribute("employee");
        List<Job> jobList = employee.getJobList();
        for (Job job:jobList) {
            if (job.getJobId()==2){
                map.put("message","成功");
            }
        }
        return map;
    }

    /**
     * 鉴别登录用户是否拥有财务员权限
     * @param session
     * @throws Exception
     */
    @RequestMapping("/identifyFinancialOfficer")
    public Map<String,Object> identifyFinancialOfficer(HttpSession session) throws Exception {
        Map<String,Object> map = new HashMap<>();
        map.put("message","");
        Employee employee = (Employee)session.getAttribute("employee");
        List<Job> jobList = employee.getJobList();
        for (Job job:jobList) {
            if (job.getJobId()==3){
                map.put("message","成功");
            }
        }
        return map;
    }



    /**
     * 财务员批量未通过订单方法
     *
     * @param ids
     * @return
     */
    @RequestMapping("/batchNoPass")
    public Map<String, Object> batchNoPass(String ids) {
        Map<String, Object> map = new HashMap<>();
        //切割选中的id字符串
        String[] split = ids.split(",");
        //装进list集合
        List<String> list = Arrays.asList(split);
        //删除客户信息表
        Integer count = orderFormService.batchNoPass(list);
        if (count != null) {
            map.put("message", "批量未通过成功！");
        } else {
            map.put("message", "批量未通过失败！");
        }
        return map;
    }





    /**
     * 财务员批量通过订单方法
     *
     * @param ids
     * @return
     */
    @RequestMapping("/batchPass")
    public Map<String, Object> batchPass(String ids) {
        Map<String, Object> map = new HashMap<>();
        //切割选中的id字符串
        String[] split = ids.split(",");
        //装进list集合
        List<String> list = Arrays.asList(split);
        //删除客户信息表
        Integer count = orderFormService.batchPass(list);
        if (count != null) {
            map.put("message", "批量通过成功！");
        } else {
            map.put("message", "批量通过失败！");
        }
        return map;
    }



    /**
     * 财务员通过单个订单的方法  可携带备注信息
     * @param map
     * @return
     */
    @RequestMapping("/pass")
    public Map<String, Object> pass(@RequestParam Map<String, Object> map) {
        //把获取到的数据进行修改  返回几条数据
        Long count = orderFormService.pass(map);
        if (count != 0) {
            map.put("message", "通过成功");
        } else {
            map.put("message", "通过失败");
        }
        return map;
    }


    /**
     * 财务员未通过单个订单的方法  可携带备注信息
     * @param map
     * @return
     */
    @RequestMapping("/noPass")
    public Map<String, Object> noPass(@RequestParam Map<String, Object> map) {
        //把获取到的数据进行修改  返回几条数据
        Long count = orderFormService.noPass(map);
        if (count != 0) {
            map.put("message", "未通过成功");
        } else {
            map.put("message", "未通过失败");
        }
        return map;
    }



    /**
     * 查询财务员权限下的需求单信息
     * @param parameterMap
     * @return
     */
    @RequestMapping("/selOrderFormForTreasurer")
    public Page selOrderFormForTreasurer(@RequestParam Map<String, Object> parameterMap) {
        //根据传入的当前页数与每页条数创建page对象
        Integer pageNow = Integer.parseInt(parameterMap.get("pageNow").toString());
        Integer pageCount = Integer.parseInt(parameterMap.get("pageCount").toString());
        //创建page对象
        Page<OrderForm> page = new Page(pageNow, pageCount);
        //查询需求单信息数量
        Long count = orderFormService.selOrderFormCountForTreasurer(parameterMap);
        page.setCount(count);
        //调用page中的方法计算出总页数
        page.computeTotal(count);
        //给map添加分页属性
        parameterMap.put("page", page);
        //查询客户信息
        List<OrderForm> OfList = orderFormService.selOrderFormForTreasurer(parameterMap);
        page.setList(OfList);
        return page;
    }






    /**
     * 修改需求单信息
     * @param map
     * @return
     */
    @RequestMapping("/updOrderForm")
    public Map<String, Object> updOrderForm(@RequestParam Map<String, Object> map) {
        //把获取到的数据进行修改  返回几条数据
        Long count = orderFormService.updOrderForm(map);
        if (count != 0) {
            map.put("message", "修改成功");
        } else {
            map.put("message", "修改失败");
        }
        return map;
    }


    /**
     * 批量删除需求单信息
     *
     * @param ids
     * @return
     */
    @RequestMapping("/delOrderForm")
    public Map<String, Object> delOrderForm(String ids) {
        Map<String, Object> map = new HashMap<>();
        //切割选中的id字符串
        String[] split = ids.split(",");
        //装进list集合
        List<String> list = Arrays.asList(split);
        //删除客户信息表
        Integer count = orderFormService.delOrderFormAll(list);
        if (count != null) {
            map.put("message", "删除" + count + "条数据");
        } else {
            map.put("message", "删除失败");
        }
        return map;
    }

    /**
     * 查询需求单信息
     * @param parameterMap
     * @return
     */
    @RequestMapping("/selOrderForm")
    public Page selOrderForm(@RequestParam Map<String, Object> parameterMap) {
        //根据传入的当前页数与每页条数创建page对象
        Integer pageNow = Integer.parseInt(parameterMap.get("pageNow").toString());
        Integer pageCount = Integer.parseInt(parameterMap.get("pageCount").toString());

        //创建page对象
        Page<OrderForm> page = new Page(pageNow, pageCount);

        //查询需求单信息数量
        Long count = orderFormService.selOrderFormCount(parameterMap);
        page.setCount(count);

        //调用page中的方法计算出总页数
        page.computeTotal(count);
        //给map添加分页属性
        parameterMap.put("page", page);
        //查询客户信息
        List<OrderForm> OfList = orderFormService.selOrderForm(parameterMap);
        page.setList(OfList);
        return page;
    }

    /**
     * 添加需求单信息
     *
     * @param map
     * @return
     */
    @RequestMapping("/addOrderForm")
    public Map<String, Object> addOrderForm(@RequestParam Map<String, Object> map) {
        //时间戳 指定的格式开头加上当前的时间作为id
        String xHOrderNo = GenerateKey.getKey("OX") + "";
        String traderNo = GenerateKey.getKey("OTd") + "";
        String treasurerNo = GenerateKey.getKey("OTr") + "";
        map.put("xHOrderNo", xHOrderNo);
        map.put("traderNo", traderNo);
        map.put("treasurerNo", treasurerNo);
        //获取物质价格
        Integer con = matterPriceService.selMatterPriceItemsNo(map);
        Integer s = Integer.parseInt(map.get("itemsPrice").toString());
        Integer itemsProfitPrice = s - con;
        map.put("itemsProfitPrice", itemsProfitPrice);
        Integer count = orderFormService.addOrderForm(map);
        if (count > 0) {
            map.put("message", "添加成功");
        } else {
            map.put("message", "添加失败");
        }
        return map;
    }

    @Autowired
    public void setOrderFormService(OrderFormService orderFormService) {
        this.orderFormService = orderFormService;
    }

    @Autowired
    public void setMatterPriceService(MatterPriceService matterPriceService) {
        this.matterPriceService = matterPriceService;
    }
}
