package com.buba.util;

import com.buba.customer.pojo.Customer;
import lombok.Data;
import org.apache.ibatis.type.Alias;

import java.util.List;

/**
 * 关于分页的类
 */
@Data
@Alias("collections")
public class Page<T> {
    private Integer pageNow;//当前页
    private Integer pageCount;//每页的条数
    private Long pageTotal;//总页数
    private Integer pageIndex;//sql中的起始下标
    private Long count;//总条数
    //结果集
    private List<T> list;

    public Page(Integer pageNow){
        this(pageNow, 5);
    }
    public Page(Integer pageNow, Integer pageCount){
        if(pageNow < 1){
            pageNow = 1;
        }
        if(pageCount < 5){
            pageCount = 5;
        }
        this.pageNow = pageNow;
        this.pageCount = pageCount;
        this.pageIndex = (pageNow-1)*pageCount;
    }

    /**
     * 根据总条数计算总页数
     */
    public void computeTotal(Long count){
        this.count = count;
        //计算总页数
        if (count%pageCount==0){
            this.pageTotal = count/pageCount;
        }else {
            this.pageTotal = count/pageCount+1;
        }
    }
}

