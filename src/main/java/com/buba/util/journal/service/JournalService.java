package com.buba.util.journal.service;

import java.util.Map;

/**
 * 日志业务层接口
 */

public interface JournalService {
    Integer addJournal(Map<String, Object> map);
}
