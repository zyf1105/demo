package com.buba.util.journal.service.impl;

import com.buba.util.journal.dao.JournalMapper;
import com.buba.util.journal.service.JournalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * 日志业务层实现类
 */
@Service
public class JournalServiceImpl implements JournalService {

    private JournalMapper journalMapper;


    @Autowired
    public void setJournalMapper(JournalMapper journalMapper) {
        this.journalMapper = journalMapper;
    }

    @Override
    public Integer addJournal(Map<String, Object> map) {
        return journalMapper.addJournal(map);
    }
}
