package com.buba.util.journal.dao;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.Map;

/**
 * 日志持久层接口
 */
@Repository
@Mapper
public interface JournalMapper {
    Integer addJournal(Map<String, Object> map);
}
