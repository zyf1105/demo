package com.buba.util;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.buba.util.journal.service.JournalService;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

/**
 * Aop切面编程
 */
@Aspect
@Component
public class WebLogAspect {

    private JournalService journalService;


    private final Logger logger = LoggerFactory.getLogger(WebLogAspect.class);

    @Pointcut("execution(public * com.buba.*.controller..*.*(..))")//切入点描述 这个是controller包的切入点
    public void controllerLog(){
        //System.out.println("拦截成功");
    }//签名，可以理解成这个切入点的一个名称

    /*@Pointcut("execution(public * com.stuPayment.uiController..*.*(..))")//切入点描述，这个是uiController包的切入点
    public void uiControllerLog(){}*/

    @Before("controllerLog()") //在切入点的方法run之前要干的
    public void logBeforeController(JoinPoint joinPoint) {
        //System.out.println("切面编程方法");

        RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();//这个RequestContextHolder是Springmvc提供来获得请求的东西
        HttpServletRequest request = ((ServletRequestAttributes)requestAttributes).getRequest();


        //获取当前的时间
        SimpleDateFormat sdf = new SimpleDateFormat();// 格式化时间
        sdf.applyPattern("yyyy-MM-dd HH:mm:ss a");// a为am/pm的标记
        Date date = new Date();// 获取当前时间
        //System.out.println("现在时间：" + sdf.format(date)); // 输出已经格式化的现在时间（24小时制）
        // 记录下请求内容
        logger.info("################URL : " + request.getRequestURL().toString());//请求路径
        logger.info("################HTTP_METHOD : " + request.getMethod());//请求方式
        logger.info("################IP : " + request.getRemoteAddr());//请求ip地址
        logger.info("################THE ARGS OF THE CONTROLLER : " + Arrays.toString(joinPoint.getArgs()));//控制器的参数
        logger.info("################Current Time : " + sdf.format(date));//当前的时间

        //下面这个getSignature().getDeclaringTypeName()是获取包+类名的   然后后面的joinPoint.getSignature.getName()获取了方法名
        logger.info("################CLASS_METHOD : " + joinPoint.getSignature().getDeclaringTypeName() + "." + joinPoint.getSignature().getName());
        //logger.info("################TARGET: " + joinPoint.getTarget());//返回的是需要加强的目标类的对象
        //logger.info("################THIS: " + joinPoint.getThis());//返回的是经过加强后的代理类的对象
//        System.out.println("id"+request.getRemoteAddr());
//        System.out.println("方法名"+joinPoint.getSignature().getName());
        Map<String,Object> map = new HashMap<>();
        map.put("journalTime",sdf.format(date));
        map.put("journalIp",request.getRemoteAddr());
        map.put("journalAction",joinPoint.getSignature().getDeclaringTypeName() + "." + joinPoint.getSignature().getName());

        //System.out.println(map);
        //调用方法给数据库添加日志
        Integer count = journalService.addJournal(map);
    }


    @Autowired
    public void setJournalService(JournalService journalService) {
        this.journalService = journalService;
    }
}
