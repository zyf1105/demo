package com.buba.util;

import java.util.Calendar;

/**
 * 关于生成表中的主键
 * 相关表首字母+年+月+日+时+分+秒+毫秒
 */
public class GenerateKey {
    /**
     * 生成主键 时间戳
     *
     * @param s 关键首字母
     * @return
     */
    public static String getKey(String s) {
        Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH) + 1;
        int date = c.get(Calendar.DAY_OF_MONTH);
        int hour = c.get(Calendar.HOUR_OF_DAY);
        int minute = c.get(Calendar.MINUTE);
        int second = c.get(Calendar.SECOND);
        int millisecond = c.get(Calendar.MILLISECOND);
        try {
            Thread.sleep(1);
        } catch (Exception e) {
        }
        // 当秒发生改变时重置自增
        return s + year + month + date + hour + minute + second + millisecond;
    }
}
