package com.buba.util;

import com.buba.goods.annotation.ExcelCell;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class Excel<T>{

    /**
     * 导入excel工具类
     * @param file
     * @throws Exception
     */
    public List<T> importExcel( MultipartFile file,Class<T> c)  {
        //创建集合储存excel中的对象
        List<T> list = new ArrayList<>();
        List<Field> declaredFields = filterField(c.getDeclaredFields());
        try {
            InputStream inputStream = file.getInputStream();
            //
//            HSSFWorkbook hssfWorkbook = new HSSFWorkbook(inputStream);

            String excelName = file.getOriginalFilename();
            Workbook hssfWorkbook = null;
            if (excelName.endsWith("xlsx")) {
                hssfWorkbook = new XSSFWorkbook(file.getInputStream());
            } else if (excelName.endsWith("xls")) {
                hssfWorkbook = new HSSFWorkbook(file.getInputStream());
            } else {
                throw new FileNotFoundException("文件类型错误");
            }

            //遍历excel中的页数
            for (int k = 0;k < hssfWorkbook.getNumberOfSheets();  k++) {
                //获取excel中的页
                HSSFSheet sheetAt = (HSSFSheet) hssfWorkbook.getSheetAt(k);
                //获取行数
                int lastRowNum = sheetAt.getLastRowNum();
                // 获取类中的所有属性信息
                for (int i = 1; i < lastRowNum; i++) {
                    //获取行中的单元格
                    HSSFRow row = sheetAt.getRow(i);
                    //创建对象装入每行的数据
                    T ob = c.getConstructor().newInstance();

                    //遍历对象中的属性集合
                    for (int j = 0; j < declaredFields.size(); j++) {
                        //判断实体类中的属性的数据类型
                        if (declaredFields.get(j).getType().getCanonicalName().contains("Integer")){
                            //调用方法获取对象的set方法并将单元格中的值赋值给对象
                            getSetMethod(ob, declaredFields.get(j).getName().toString(),Double.valueOf(row.getCell(j).toString()).intValue());
                        }
                        if (declaredFields.get(j).getType().getCanonicalName().contains("String")){
                            //调用方法获取对象的set方法并将单元格中的值赋值给对象
                            getSetMethod(ob, declaredFields.get(j).getName().toString(),row.getCell(j).toString());
                        }
                        if (declaredFields.get(j).getType().getCanonicalName().contains("Long")){
                            //调用方法获取对象的set方法并将单元格中的值赋值给对象
                            getSetMethod(ob, declaredFields.get(j).getName().toString(),Long.valueOf(row.getCell(j).toString()));
                        }
                        if (declaredFields.get(j).getType().getCanonicalName().contains("Float")){
                            //调用方法获取对象的set方法并将单元格中的值赋值给对象
                            getSetMethod(ob, declaredFields.get(j).getName().toString(),Float.valueOf(row.getCell(j).toString()));
                        }
                        if (declaredFields.get(j).getType().getCanonicalName().contains("Short")){
                            //调用方法获取对象的set方法并将单元格中的值赋值给对象
                            getSetMethod(ob, declaredFields.get(j).getName().toString(),Short.valueOf(row.getCell(j).toString()));
                        }
                        if (declaredFields.get(j).getType().getCanonicalName().contains("Double")){
                            //调用方法获取对象的set方法并将单元格中的值赋值给对象
                            getSetMethod(ob, declaredFields.get(j).getName().toString(),Double.valueOf(row.getCell(j).toString()));
                        }
                        if (declaredFields.get(j).getType().getCanonicalName().contains("BigDecimal")){
                            //调用方法获取对象的set方法并将单元格中的值赋值给对象
                            getSetMethod(ob, declaredFields.get(j).getName().toString(),new BigDecimal(row.getCell(j).toString()));
                        }
                    }
                    //把对象添加到集合中
                    list.add(ob);
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return list;
    }

    /**
     * 导出Excel
     *
     * @param excelName 导出页名字
     * @param dataList  导出的数据
     * @param c         要导出的对象类
     * @param response  下载需要的响应对象
     * @throws Exception
     */
    public void exportExcel(String excelName, List<T> dataList, Class<T> c, HttpServletResponse response) throws Exception {
        //创建HSSFWorkbook对象 poi操作Excel对象
        HSSFWorkbook wb = new HSSFWorkbook();
        //建立sheet对象
        HSSFSheet sheet = wb.createSheet(excelName);

//------------------ 设置Excel表头信息
        // 获取类中的所有属性信息
        Field[] declaredFields = c.getDeclaredFields();
        System.out.println(declaredFields);
        //创建Excel的表头 创建行Row
        HSSFRow firstRow = sheet.createRow(0);
        // 获取要导出的类中有多少个属性
        int cellLength = declaredFields.length;
        for (int j = 0; j < cellLength; j++) {
            // 在行中创建单元格
            HSSFCell cell = firstRow.createCell(j);
            // 把属性名上标注的注解写入到单元格中
            cell.setCellValue(String.valueOf(declaredFields[j].getAnnotation(ExcelCell.class).value()));
         }

//------------------ 关于导出的具体数据
        // 编写Excel的内容 遍历List集合
        for (int i = 0; i < dataList.size(); i++) {
            // 根据对象的数量创建行
            HSSFRow row = sheet.createRow(i + 1);
            // 获取该行需要写入的对象
            T t = dataList.get(i);
            // 根据对象的属性循环
            for (int j = 0; j < cellLength; j++) {
                // 根据属性的数量创建单元格
                HSSFCell cell = row.createCell(j);
                //判断该单元格数据是否为空
                if (getGetMethod(t, declaredFields[j].getName()) == null) {
                    cell.setCellValue("");
                    break;
                }
                // 向单元格中添加值 添加的是对象的get方法
                cell.setCellValue(getGetMethod(t, declaredFields[j].getName()).toString());
            }
        }

        //输出Excel文件
        OutputStream output = response.getOutputStream();
        response.reset();
        //设置响应头，
        response.setHeader("Content-disposition", "attachment; filename=" + c.getSimpleName() + ".xls");
        response.setContentType("application/msexcel");
        wb.write(output);
        output.close();
    }


    /**
     * 根据属性，获取set方法
     * @param obj  对象
     * @param name 属性名
     * @return  该方法返回值
     * @throws Exception
     */
    public Object getSetMethod(Object obj, String name,Object val) throws Exception {
        // 获取该类中所有的方法 get set toString
        Method[] declaredMethod  = obj.getClass().getMethods();
        for (int i = 0; i < declaredMethod .length; i++) {
            // get+属性名
            if (("set" + name).toLowerCase().equals(declaredMethod [i].getName().toLowerCase())) {
                // 执行该对象的get方法 方法.invoke(对象)
                return declaredMethod [i].invoke(obj,val);
            }
        }
        return null;
    }



    /**
     * 根据属性，获取get方法
     * @param obj  对象
     * @param name 属性名
     * @return  该方法返回值
     * @throws Exception
     */
    public Object getGetMethod(Object obj, String name) throws Exception {
        // 获取该类中所有的方法 get set toString
        Method[] m = obj.getClass().getMethods();
        for (int i = 0; i < m.length; i++) {
            // get+属性名
            if (("get" + name).toLowerCase().equals(m[i].getName().toLowerCase())) {
                // 执行该对象的get方法 方法.invoke(对象)
                return m[i].invoke(obj);
            }
        }
        return null;
    }

    /**
     * 字段过滤
     *
     * @param fields
     * @return
     */
    private List<Field> filterField(Field[] fields) {
        List<Field> list = new ArrayList<>();
        for (Field field : fields) {
            if (field.isAnnotationPresent(ExcelCell.class))
                list.add(field);
        }
        return list;
    }
}
