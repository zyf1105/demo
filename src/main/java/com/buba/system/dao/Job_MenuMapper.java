package com.buba.system.dao;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.Map;

@Repository
@Mapper
public interface Job_MenuMapper {
    Integer addJob_Menu(Map<String, Object> map);

    Integer delJob_MenuByJobId(Map<String, Object> map);
}
