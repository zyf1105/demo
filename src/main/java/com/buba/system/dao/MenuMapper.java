package com.buba.system.dao;

import com.buba.system.pojo.Menu;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface MenuMapper {
    List<Menu> selMenu(Integer jobId);

    List<Menu> selMenuAll();
}
