package com.buba.system.dao;

import com.buba.system.pojo.Emp_Job;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Mapper
@Repository
public interface Emp_JobMapper {
    Long addEmp_job(List<Emp_Job> list);

    Integer delEmp_job(List<String> list);

    Integer addEmp_jobs(Map<String, Object> parameterMap);

    Integer delEmp_JobByJobId(String jobId);
}
