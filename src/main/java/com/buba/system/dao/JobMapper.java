package com.buba.system.dao;

import com.buba.system.pojo.Job;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Mapper
@Repository
public interface JobMapper {
    List<Job> selJobById(Integer employeeId);

    List<Job> selJob();

    Long addJob(Map<String, Object> map);

    Integer delJobById(String jobId);
}
