package com.buba.system.dao;

import com.buba.system.pojo.SystemUser;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
@Mapper
public interface SystemUserMapper {
    SystemUser selSystemUser(Map<String, Object> map);

    Integer updPassword(SystemUser systemUser);

    Long addSystemUser(SystemUser systemUser);

    Integer delSystemUser(List<String> list);

    Integer updSystemUser(SystemUser systemUser);
}
