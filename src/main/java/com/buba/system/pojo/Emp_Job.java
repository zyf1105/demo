package com.buba.system.pojo;

import lombok.Data;

/**
 * 关联表实体类
 */
@Data
public class Emp_Job {
    private Integer emp_job_id;
    private Integer employeeId;
    private Integer jobId;
}
