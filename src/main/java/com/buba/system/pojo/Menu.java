package com.buba.system.pojo;

import lombok.Data;






/**
 * 菜单实体类
 */
@Data
public class Menu implements Comparable<Menu>{
    private Integer menuId;
    private String menuName;
    private String menuUrl;
    private Integer menuPid;
    private boolean open = false;
    private boolean checked = false;


    @Override
    public int compareTo(Menu o) {
        if(this.getMenuId() > o.getMenuId()){
            return 1;
        }
        if(this.getMenuId() < o.getMenuId()){
            return -1;
        }
        return 0;
    }
}
