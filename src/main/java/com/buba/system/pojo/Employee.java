package com.buba.system.pojo;

import lombok.Data;

import java.util.List;

/**
 * 员工类
 */
@Data
public class Employee {
    private Integer employeeId;
    private String employeeName;
    private Integer employeeSex;
    private String employeeIdCard;
    private String employeePhoneNumer;
    private String employeeAddress;

    //关于员工账号信息 用于修改密码
    private SystemUser systemUser;
    //一个员工可能身兼数职
    private List<Job> jobList;
    //拥有的菜单权限
    private List<Menu> menuList;



}
