package com.buba.system.pojo;

import lombok.Data;

import java.util.List;

/**
 * 职位对象
 */
@Data
public class Job {
    private  Integer jobId;
    private  String JobTitle;
    //一个职位拥有多个菜单权限
    private List<Menu> menuList;
}
