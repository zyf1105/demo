package com.buba.system.pojo;

import lombok.Data;

/**
 * 系统用户实体类
 */
@Data
public class SystemUser {
    private Integer userId;
    private Integer employeeId;
    private String account;
    private String password;

}
