package com.buba.system.controller;

import com.buba.system.pojo.Employee;
import com.buba.system.pojo.Job;
import com.buba.system.pojo.Menu;
import com.buba.system.pojo.SystemUser;
import com.buba.system.service.EmployeeService;
import com.buba.system.service.JobService;
import com.buba.system.service.MenuService;
import com.buba.system.service.SystemUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 系统用户控制层
 */
@RestController
@RequestMapping("/SystemUser")
public class SystemUserController {
    private SystemUserService systemUserService;
    private EmployeeService employeeService;
    private JobService jobService;
    private MenuService menuService;

    /**
     * 获取session中的数据
     * @return
     */
    @RequestMapping("/updPassword")
    public  Map<String,Object> updPassword(HttpSession session,SystemUser systemUser){
        Map<String,Object> map = new HashMap<>();
        //调用业务层的修改方法
        Integer count = systemUserService.updPassword(systemUser);
        if (count!=0){
            map.put("msg","");
            //密码修改成功后清除session中的数据
            session.invalidate();
        }else{
            map.put("msg","修改失败");
        }
        return map;
    }
    /**
     * 获取session中的数据
     * @return
     */
    @RequestMapping("/selSession")
    public  Map<String,Employee> selSession(HttpSession session){
        Map<String,Employee> map = new HashMap<>();
        Employee employee = (Employee)session.getAttribute("employee");
//        System.out.println("session中的数据"+employee);
        map.put("employee",employee);
        return map;
    }
    /**
     * 系统用户登录方法
     * @return
     */
    @RequestMapping("/login")
    public Map<String,Object> selSystemUser(HttpSession httpSession, String account, String password){
        Map<String,Object> map = new HashMap<>();
        map.put("account",account );
        map.put("password",password);
        SystemUser systemUser = systemUserService.selSystemUser(map);
        //判断是否存在该用户
        if(systemUser!= null){
            //查询该用户拥有的菜单权限以及个人信息
            //根据登录成功账号的的员工外键查询对应的信息
            Employee employee = employeeService.selEmployeeById(systemUser.getEmployeeId());
            employee.setSystemUser(systemUser);

            //查询职位信息
            List<Job> jobList = jobService.selJobById(systemUser.getEmployeeId());
            //把职业数据装入员工对象中
            employee.setJobList(jobList);
            //查询拥有的职业对应的权限
            for (Job job: jobList) {
                List<Menu> menuList = menuService.selMenu(job.getJobId());
                job.setMenuList(menuList);
            }
            //调用业务层的去重方法去掉重复的权限
            List<Menu> menuList = menuService.noDup(jobList);

            //把处理好的权限数据装入员工对象中
            employee.setMenuList(menuList);

//            System.out.println(employee.getJobList());
//            System.out.println(employee.getMenuList());
            //向session中存储数据
            httpSession.setAttribute("employee",employee);

            map.put("msg","");
        }else {
            map.put("msg","账号或密码错误!");
        }
        return  map;
    }

    @Autowired
    public void setMenuService(MenuService menuService) {
        this.menuService = menuService;
    }

    @Autowired
    public void setJobService(JobService jobService) {
        this.jobService = jobService;
    }

    @Autowired
    public void setEmployeeService(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }

    @Autowired
    public void setSystemUserService(SystemUserService systemUserService) {
        this.systemUserService = systemUserService;
    }
}
