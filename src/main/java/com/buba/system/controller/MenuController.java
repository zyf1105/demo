package com.buba.system.controller;

import com.alibaba.fastjson.JSON;
import com.buba.system.pojo.Menu;
import com.buba.system.service.Job_MenuService;
import com.buba.system.service.MenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/MenuController")
public class MenuController {
    private MenuService menuService;
    private Job_MenuService job_menuService;


    /**
     * 修改职业与权限的关联表
     * @return
     */
    @RequestMapping("/updMenu")
    public Map<String ,Object> updMenu(String treeNodes,String jobId){
        System.out.println(jobId);
        Map<String ,Object> map = new HashMap<>();
        //先删除关联表中的数据
        map.put("jobId",jobId);
        System.out.println(map);
        Integer count = job_menuService.delJob_MenuByJobId(map);

        //转换前端的字符串为集合
        List<Menu> menuList = JSON.parseArray(treeNodes, Menu.class);
        map.put("menuList",menuList);

        Integer count2 = job_menuService.addJob_Menu(map);
        if (count2 != 0){
            map.put("msg","修改成功！");
        }

        return map;
    }




    @RequestMapping("/selMenuAll")
    public Map<String ,Object> selMenuAll(){
        Map<String ,Object> map = new HashMap<>();
        List<Menu> list = menuService.selMenuAll();

        System.out.println(list);
        map.put("list",list);
        return map;
    }


    @RequestMapping("/selMenuByJobId")
    public Map<String ,Object> selMenuByJobId(Integer jobId){
        Map<String ,Object> map = new HashMap<>();
        List<Menu> lists = menuService.selMenuAll();
        List<Menu> list = menuService.selMenu(jobId);
        list.forEach(s ->{
            s.setChecked(true);
        } );


        for (int i = 0; i <lists.size() ; i++) {
            for (int j = 0; j < list.size(); j++) {
                if (list.get(j).getMenuId() == lists.get(i).getMenuId()) {
                    lists.get(i).setChecked(true);
                }
            }
        }
        map.put("list",lists);
        return map;
    }


    @Autowired
    public void setJob_menuService(Job_MenuService job_menuService) {
        this.job_menuService = job_menuService;
    }

    @Autowired
    public void setMenuService(MenuService menuService) {
        this.menuService = menuService;
    }
}
