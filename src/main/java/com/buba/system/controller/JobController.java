package com.buba.system.controller;

import com.alibaba.fastjson.JSON;
import com.buba.system.pojo.Employee;
import com.buba.system.pojo.Job;
import com.buba.system.pojo.Menu;
import com.buba.system.service.Emp_JobService;
import com.buba.system.service.JobService;
import com.buba.system.service.Job_MenuService;
import com.buba.system.service.MenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.util.*;

/**
 *职业类控制层
 */
@RestController
@RequestMapping("/JobController")
public class JobController {
    private JobService jobService;
    private MenuService menuService;
    private Job_MenuService job_MenuService;
    private Emp_JobService emp_jobService;


    /**
     * 删除职位方法
     * @return
     */
    @RequestMapping("/delJobById")
    public Map<String ,Object> delJobById(String jobId){
        Map<String ,Object> map = new HashMap<>();
        map.put("jobId",jobId);
        //删除关联表Emp_Job中的数据
        Integer count = emp_jobService.delEmp_JobByJobId(jobId);

        Integer count2 = job_MenuService.delJob_MenuByJobId(map);
        if (count2 != 0){
            Integer count3 = jobService.delJobById(jobId);
            if (count3 != 0){
                map.put("msg","删除成功！");
            }else{
                map.put("msg","删除失败！");
            }
        }else{
            map.put("msg","删除失败！");
        }

        return map;
    }



    /**
     * 添加职位方法
     * @return
     */
    @RequestMapping("/addJob")
    public Map<String , Object> addJob(String jobTitle,String treeNodes){
        Map<String ,Object> map = new HashMap<>();
        map.put("jobTitle",jobTitle);
        //调用业务层添加职业信息
        Long count = jobService.addJob(map);
        System.out.println(map);
        if (count !=0){
            //转换前端的字符串为集合
            List<Menu> menuList = JSON.parseArray(treeNodes, Menu.class);
            map.put("menuList",menuList);
            System.out.println(map);
            Integer count2 = job_MenuService.addJob_Menu(map);
            if(count2 != 0){
                map.put("msg","添加成功！");
            }else {
                map.put("msg","添加失败！");
            }
        }else {
            map.put("msg","添加失败！");
        }



        return map;
    }



    /**
     * 查询职位方法
     * @return
     */
    @RequestMapping("/selJob")
    public Map<String ,Object> selJob(){
        Map<String ,Object> map = new HashMap<>();
        List<Job> list = jobService.selJob();
        map.put("list",list);
        return map;
    }

    /**
     * 根据用工id查询职位方法
     * @return
     */
    @RequestMapping("/selJobByEmployeeId")
    public Employee selJobByEmployeeId(HttpSession session){
        Employee emp = new Employee();
        Employee employee = (Employee)session.getAttribute("employee");
        Integer employeeId =employee.getEmployeeId();
        List<Job> list = jobService.selJobById(employeeId);
        //循环jobList查询对应的权限
        for (Job job: list) {
            //创建集合装入权限数据
            List<Menu> menuList = menuService.selMenu(job.getJobId());
            job.setMenuList(menuList);
        }
        //调用去重方法
        List<Menu> menuList = menuService.noDup(list);
        menuList.forEach(s -> {
            s.setChecked(true);
        });

        emp.setMenuList(menuList);

        emp.setJobList(list);
        return emp;
    }

    @Autowired
    public void setEmp_jobService(Emp_JobService emp_jobService) {
        this.emp_jobService = emp_jobService;
    }

    @Autowired
    public void setJob_MenuService(Job_MenuService job_MenuService) {
        this.job_MenuService = job_MenuService;
    }

    @Autowired
    public void setMenuService(MenuService menuService) {
        this.menuService = menuService;
    }

    @Autowired
    public void setJobService(JobService jobService) {
        this.jobService = jobService;
    }
}
