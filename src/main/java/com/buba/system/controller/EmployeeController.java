package com.buba.system.controller;

import com.buba.system.pojo.Emp_Job;
import com.buba.system.pojo.Employee;
import com.buba.system.pojo.Page;
import com.buba.system.pojo.SystemUser;
import com.buba.system.service.Emp_JobService;
import com.buba.system.service.EmployeeService;
import com.buba.system.service.JobService;
import com.buba.system.service.SystemUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

/**
 * 员工类控制层
 */
@RestController
@RequestMapping("/EmployeeController")
public class EmployeeController {
    private EmployeeService employeeService;
    private JobService jobService;
    private Emp_JobService emp_jobService;
    private SystemUserService systemUserService;


    /**
     * 删除员工方法
     * @return
     */
    @RequestMapping("/delEmployee")
    public Map<String,Object> delEmployee(String empIds){
        System.out.println(empIds);
        Map<String,Object> map = new HashMap<>();
        //前端id字符串转为数组
        String[] split = empIds.split(",");
        //数组转为list集合
        List<String> list = Arrays.asList(split);

        //调用业务层的删除方法
        Integer count = employeeService.delEmployee(list);

        //如果删除员工表成功则继续执行删除
        if (count!=0){
            //删除关联表
            Integer count1 = emp_jobService.delEmp_job(list);

            //删除登录表信息
            Integer count2 = systemUserService.delSystemUser(list);

            if (count1!=0 && count2!=0){
                map.put("msg","删除成功！");
            }else {
                map.put("msg","关联表或账号表删除失败！");
            }
        }else{
            map.put("msg","删除失败！");
        }
        return map;
    }


    /**
     * 修改员工方法
     * @return
     */
    @RequestMapping("/updEmp")
    public Map<String,Object> updEmp(@RequestParam Map<String,Object> parameterMap){
        System.out.println(parameterMap);
        //修改关联表的数据 先删除关联表的数据  然后再添加新的数据
        //根据id删除数据
        //获取添加的员工id值
        String employeeId = parameterMap.get("employeeId").toString();
        List<String> list = new ArrayList<>();
        list.add(employeeId);
        //删除关联表
        Integer count1 = emp_jobService.delEmp_job(list);


            String value = parameterMap.get("value2").toString();
            String[] jobList = value.split(",");
            parameterMap.put("jobList",jobList);

            //执行添加
            Integer count2 = emp_jobService.addEmp_jobs(parameterMap);
            if (count2!=0){
                //修改员工表
                Integer count3 = employeeService.updEmp(parameterMap);

                //修改账号表
                //添加账号信息
                //创建登录账号对象
                //获取添加的员工id值
                Integer eId = Integer.parseInt(parameterMap.get("employeeId").toString());
                SystemUser systemUser = new SystemUser();
                systemUser.setEmployeeId(eId);
                systemUser.setAccount(parameterMap.get("employeeIdCard").toString());
                systemUser.setPassword(parameterMap.get("employeeIdCard").toString().substring(0,5));
                Integer count4 = systemUserService.updSystemUser(systemUser);
                if (count3!=0 && count4!=0){
                    parameterMap.put("msg","修改成功！");
                }else{
                    parameterMap.put("msg","员工表或者账号表修改失败！");
                }
            }else {
                parameterMap.put("msg","关联表执行添加失败！");
            }
        return parameterMap;
    }




    /**
     * 添加员工方法
     * @return
     */
    @RequestMapping("/addEmp")
    public Map<String,Object> addEmp(@RequestParam Map<String,Object> parameterMap){

        Long count = employeeService.addEmp(parameterMap);
        if (count!=0){
            //获取添加的员工id值
            Integer employeeId = Integer.parseInt(parameterMap.get("employeeId").toString());
            //获取前端传入的职业id数组
            String value = parameterMap.get("value").toString();
            String[] chrstr=value.split(",");

            //创建list集合转入emp_job中间表的对象
            List<Emp_Job> list = new ArrayList<>();
            //循环list获取里边的数据
            for (String id:chrstr) {
                //把集合中的对象转为数字
                Integer jobId = Integer.parseInt(id.toString());
                //创建中间表对象
                Emp_Job emp_job = new Emp_Job();
                //给对象添加数据
                emp_job.setEmployeeId(employeeId);
                emp_job.setJobId(jobId);
                //将对象装入集合中
                list.add(emp_job);
            }

            Long count1 = emp_jobService.addEmp_job(list);
            //添加账号信息
            //创建登录账号对象
            SystemUser systemUser = new SystemUser();
            systemUser.setEmployeeId(employeeId);
            systemUser.setAccount(parameterMap.get("employeeIdCard").toString());
            systemUser.setPassword(parameterMap.get("employeeIdCard").toString().substring(0,5));

            //执行添加账号信息
            Long count2 = systemUserService.addSystemUser(systemUser);
            parameterMap.clear();
            if (count1!=0 && count2!=0){

                parameterMap.put("msg","添加成功!");
            }else{
                parameterMap.put("msg","职业信息或账号信息添加失败!");
            }
        }
        else{
            parameterMap.put("msg","添加失败!");
        }
        return parameterMap;
    }


    /**
     * 查询员工信息
     */
    @RequestMapping("/selEmployee")
    public Page selEmployee(@RequestParam Map<String,Object> map){
        Page<Employee> page = new Page<>(Integer.parseInt(map.get("pageNow").toString()));
        map.put("page", page);
        String jobTitle = map.get("jobTitle").toString();
        String employeeName = map.get("employeeName").toString();
        List<Employee> list = null;
        // 没有根据职位进行查询 查询所有员工
        if ("".equals(jobTitle) && "".equals(employeeName)) {
            // 没有条件查询
            page.computeTotal(employeeService.selCountAll());
            list = employeeService.selEmployeeAll(page);
        } else { // 携带查询条件
            page.computeTotal(employeeService.selEmployeeCount(map));
            list = employeeService.selEmployee(map);
        }
        list.forEach(e -> {
            e.setJobList(jobService.selJobById(e.getEmployeeId()));
        });
        page.setEmployeeList(list);
        return page;
    }

    @Autowired
    public void setSystemUserService(SystemUserService systemUserService) {
        this.systemUserService = systemUserService;
    }

    @Autowired
    public void setEmp_jobService(Emp_JobService emp_jobService) {
        this.emp_jobService = emp_jobService;
    }

    @Autowired
    public void setJobService(JobService jobService) {
        this.jobService = jobService;
    }

    @Autowired
    public void setEmployeeService(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }
}
