package com.buba.system.service;

import com.buba.system.pojo.Job;

import java.util.List;
import java.util.Map;

public interface JobService {
    List<Job> selJobById(Integer employeeId);

    List<Job> selJob();

    Long addJob(Map<String, Object> map);

    /**
     * 根据id删除职位
     * @param jobId
     * @return
     */
    Integer delJobById(String jobId);
}
