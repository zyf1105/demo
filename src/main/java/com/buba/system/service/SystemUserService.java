package com.buba.system.service;

import com.buba.system.pojo.SystemUser;

import java.util.List;
import java.util.Map;

public interface SystemUserService {
    SystemUser selSystemUser(Map<String, Object> map);

    Integer updPassword(SystemUser systemUser);

    Long addSystemUser(SystemUser systemUser);

    Integer delSystemUser(List<String> list);

    Integer updSystemUser(SystemUser systemUser);
}
