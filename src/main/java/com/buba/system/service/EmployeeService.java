package com.buba.system.service;

import com.buba.system.pojo.Employee;
import com.buba.system.pojo.Page;

import java.util.List;
import java.util.Map;

public interface EmployeeService {
    Employee selEmployeeById(Integer employeeld);

    List<Employee> selEmployee(Map<String, Object> map);

    Long selEmployeeCount(Map<String, Object> map);


    Long addEmp(Map<String, Object> parameterMap);

    Integer delEmployee(List<String> list);

    Integer updEmp(Map<String, Object> parameterMap);

    Long selCountAll();

    List<Employee> selEmployeeAll(Page<Employee> page);
}
