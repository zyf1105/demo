package com.buba.system.service.impl;

import com.buba.system.dao.Emp_JobMapper;
import com.buba.system.pojo.Emp_Job;
import com.buba.system.service.Emp_JobService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class Emp_JobServiceImpl implements Emp_JobService {
    private Emp_JobMapper emp_jobMapper;


    @Autowired
    public void setEmp_jobMapper(Emp_JobMapper emp_jobMapper) {
        this.emp_jobMapper = emp_jobMapper;
    }

    @Override
    public Long addEmp_job(List<Emp_Job> list) {
        return emp_jobMapper.addEmp_job(list);
    }

    @Override
    public Integer delEmp_job(List<String> list) {
        return emp_jobMapper.delEmp_job(list);
    }

    @Override
    public Integer addEmp_jobs(Map<String, Object> parameterMap) {
        return emp_jobMapper.addEmp_jobs(parameterMap);
    }

    @Override
    public Integer delEmp_JobByJobId(String jobId) {
        return emp_jobMapper.delEmp_JobByJobId( jobId);
    }
}
