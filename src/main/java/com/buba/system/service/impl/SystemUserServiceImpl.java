package com.buba.system.service.impl;

import com.buba.system.dao.SystemUserMapper;
import com.buba.system.pojo.SystemUser;
import com.buba.system.service.SystemUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class SystemUserServiceImpl implements SystemUserService {
    private SystemUserMapper systemUserMapper;
    @Override
    public SystemUser selSystemUser(Map<String, Object> map) {
        return systemUserMapper.selSystemUser(map);
    }

    @Override
    public Integer updPassword(SystemUser systemUser) {
        return systemUserMapper.updPassword( systemUser);
    }

    @Override
    public Long addSystemUser(SystemUser systemUser) {
        return systemUserMapper.addSystemUser( systemUser);
    }

    @Override
    public Integer delSystemUser(List<String> list) {
        return systemUserMapper.delSystemUser(list);
    }

    @Override
    public Integer updSystemUser(SystemUser systemUser) {
        return systemUserMapper.updSystemUser(systemUser);
    }

    @Autowired
    public void setSystemUserMapper(SystemUserMapper systemUserMapper) {
        this.systemUserMapper = systemUserMapper;
    }
}
