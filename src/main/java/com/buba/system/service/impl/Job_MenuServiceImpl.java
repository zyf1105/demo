package com.buba.system.service.impl;

import com.buba.system.dao.Job_MenuMapper;
import com.buba.system.service.Job_MenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class Job_MenuServiceImpl implements Job_MenuService {
    private Job_MenuMapper job_menuMapper;



    @Autowired
    public void setJob_menuMapper(Job_MenuMapper job_menuMapper) {
        this.job_menuMapper = job_menuMapper;
    }

    @Override
    public Integer addJob_Menu(Map<String, Object> map) {
        return job_menuMapper.addJob_Menu(map);
    }

    @Override
    public Integer delJob_MenuByJobId(Map<String, Object> map) {
        return job_menuMapper.delJob_MenuByJobId(map);
    }
}
