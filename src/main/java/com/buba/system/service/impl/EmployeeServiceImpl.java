package com.buba.system.service.impl;

import com.buba.system.dao.EmployeeMapper;
import com.buba.system.pojo.Employee;
import com.buba.system.pojo.Page;
import com.buba.system.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class EmployeeServiceImpl implements EmployeeService {
    private EmployeeMapper employeeMapper;


    @Autowired
    public void setEmployeeMapper(EmployeeMapper employeeMapper) {
        this.employeeMapper = employeeMapper;
    }

    @Override
    public Employee selEmployeeById(Integer employeeld) {
        return employeeMapper.selEmployeeById( employeeld);
    }


    @Override
    public List<Employee> selEmployee(Map<String, Object> map) {
        return employeeMapper.selEmployee( map);
    }

    @Override
    public Long selEmployeeCount(Map<String, Object> map) {
        return employeeMapper.selEmployeeCount( map);
    }

    @Override
    public Long addEmp(Map<String, Object> parameterMap) {
        return employeeMapper.addEmp( parameterMap);
    }

    @Override
    public Integer delEmployee(List<String> list) {
        return employeeMapper.delEmployee(list);
    }

    @Override
    public Integer updEmp(Map<String, Object> parameterMap) {
        return employeeMapper.updEmp(parameterMap);
    }

    @Override
    public Long selCountAll() {
        return employeeMapper.selCountAll();
    }

    @Override
    public List<Employee> selEmployeeAll(Page<Employee> page) {
        return employeeMapper.selEmployeeAll(page);
    }
}
