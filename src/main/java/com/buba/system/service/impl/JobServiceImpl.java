package com.buba.system.service.impl;

import com.buba.system.dao.JobMapper;
import com.buba.system.pojo.Job;
import com.buba.system.service.JobService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class JobServiceImpl implements JobService {
    private JobMapper jobMapper;

    @Autowired
    public void setJobMapper(JobMapper jobMapper) {
        this.jobMapper = jobMapper;
    }

    @Override
    public List<Job> selJobById(Integer employeeId) {

        return jobMapper.selJobById( employeeId);
    }

    @Override
    public List<Job> selJob() {
        return jobMapper.selJob();
    }

    @Override
    public Long addJob(Map<String, Object> map) {
        return jobMapper.addJob(map);
    }

    @Override
    public Integer delJobById(String jobId) {
        return jobMapper.delJobById(jobId);
    }


}
