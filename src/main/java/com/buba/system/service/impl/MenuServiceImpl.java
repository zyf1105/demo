package com.buba.system.service.impl;

import com.buba.system.dao.MenuMapper;
import com.buba.system.pojo.Job;
import com.buba.system.pojo.Menu;
import com.buba.system.service.MenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class MenuServiceImpl implements MenuService {
    private MenuMapper menuMapper;
    @Override
    public List<Menu> selMenu(Integer jobId) {
        return menuMapper.selMenu(jobId);
    }

    @Override
    public List<Menu> noDup(List<Job> jobList) {
        //使用set集合去重时 需要考虑是根据内容还是内存的虚地址去重
        //根据内容去重时 必须给对象重写equals() 方法 hashCode（）方法
        Set<Menu> menuSet = new TreeSet<>();
        for (Job job: jobList) {
            //遍历该职位拥有的菜单
            for (Menu menu:job.getMenuList()) {
                //把对象放入set集合中
                menuSet.add(menu);
            }
        }
        List<Menu> menuList= new ArrayList<>() ;
        for (Menu menu:menuSet) {
            menuList.add(menu);
        }
        return menuList;
    }

    @Override
    public List<Menu> selMenuAll() {
        return menuMapper.selMenuAll();
    }




    @Autowired
    public void setMenuMapper(MenuMapper menuMapper) {
        this.menuMapper = menuMapper;
    }
}
