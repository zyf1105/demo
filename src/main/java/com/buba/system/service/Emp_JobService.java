package com.buba.system.service;

import com.buba.system.pojo.Emp_Job;

import java.util.List;
import java.util.Map;

public interface Emp_JobService {
    Long addEmp_job(List<Emp_Job> list);

    Integer delEmp_job(List<String> list);

    Integer addEmp_jobs(Map<String, Object> parameterMap);

    Integer delEmp_JobByJobId(String jobId);
}
