package com.buba.system.service;

import java.util.Map;

public interface Job_MenuService {

    Integer addJob_Menu(Map<String, Object> map);

    Integer delJob_MenuByJobId(Map<String, Object> map);
}
