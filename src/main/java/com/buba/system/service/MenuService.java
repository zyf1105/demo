package com.buba.system.service;

import com.buba.system.pojo.Job;
import com.buba.system.pojo.Menu;

import java.util.List;

public interface MenuService {
    List<Menu> selMenu(Integer jobId);
    List<Menu> noDup(List<Job> jobList);

    List<Menu> selMenuAll();

}
