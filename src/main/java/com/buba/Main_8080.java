package com.buba;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 启动类
 */
@SpringBootApplication
public class Main_8080 {
    public static void main(String[] args) {
        SpringApplication.run(Main_8080.class,args);
    }
}
