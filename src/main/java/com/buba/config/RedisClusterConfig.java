package com.buba.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import redis.clients.jedis.HostAndPort;
import redis.clients.jedis.JedisCluster;
import redis.clients.jedis.JedisPoolConfig;

import java.util.HashSet;
import java.util.Set;

/**
 * springIOC/DI管理类
 *
 */
@Configuration
public class RedisClusterConfig  {
   /* @Value("${spring.redis.cluster.nodes}")
    private String clusterNodes;

    @Value("${spring.redis.database}")
    private int database;
    @Value("${spring.redis.timeout}")
    private int timeout;

    @Value("${spring.redis.pool.max-idle}")
    private int maxIdle;

    @Value("${spring.redis.pool.min-idle}")
    private int minIdle;

    @Value("${spring.redis.pool.max-active}")
    private int maxActive;

    @Value("${spring.redis.pool.max-wait}")
    private int maxWait;
*/

    @Bean
    public JedisCluster getJedisCluster() {
        return new JedisCluster(getNodes(),3000, getPoolConfig());
    }


    private JedisPoolConfig getPoolConfig() {
        JedisPoolConfig config = new JedisPoolConfig();
        config.setMaxIdle( 8);
        config.setMaxTotal( 8);
        config.setMaxWaitMillis( 3000);
        return config;
    }

    private Set<HostAndPort> getNodes() {
        Set<HostAndPort> nodes = new HashSet<HostAndPort>();
        nodes.add(new HostAndPort("192.168.234.128",6379));
        nodes.add(new HostAndPort("192.168.234.128",6380));
        nodes.add(new HostAndPort("192.168.234.128",6381));
        nodes.add(new HostAndPort("192.168.234.128",6382));
        return nodes;
    }


}
