package com.buba.reportForm.controller;

import com.buba.reportForm.pojo.ItemsType;
import com.buba.reportForm.service.ItemsTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 货物种类控制层
 */
@RestController
@RequestMapping("/ItemsTypeController")
public class ItemsTypeController {

    private ItemsTypeService itemsTypeService;


    /**
     * 查询货物种类及数量方法
     */
    @RequestMapping("/selItemsType")
    public Map<String,Object> selItemsType(){
        Map<String,Object> map = new HashMap<>();
        List<ItemsType> list = itemsTypeService.selItemsType();
        List<ItemsType> list2 = itemsTypeService.selItemsTypeAndCounts();
        for (ItemsType itemsType:list) {
            Integer count = 0;
            for (ItemsType itemsType2:list2) {
                if (itemsType.getItemsTypeId().equals(itemsType2.getItemsTypeId())){
                    count+=itemsType2.getItemsCounts();
                }
            }
            itemsType.setItemsAllCounts(count);
            System.out.println(itemsType);
        }

        map.put("list",list);
        return map;
    }


    @Autowired
    public void setItemsTypeService(ItemsTypeService itemsTypeService) {
        this.itemsTypeService = itemsTypeService;
    }
}
