package com.buba.reportForm.pojo;

import lombok.Data;

/**
 * 货物种类实体类
 */
@Data
public class ItemsType {
    private String itemsTypeId;
    private String ItemsTypeName;
    private Integer itemsCounts;
    private Integer itemsAllCounts;
}
