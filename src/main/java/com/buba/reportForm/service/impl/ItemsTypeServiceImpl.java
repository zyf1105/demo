package com.buba.reportForm.service.impl;

import com.buba.reportForm.dao.ItemsTypeMapper;
import com.buba.reportForm.pojo.ItemsType;
import com.buba.reportForm.service.ItemsTypeService;
import com.buba.system.pojo.Job;
import com.buba.system.pojo.Menu;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

/**
 * 货物种类业务层实现类
 */
@Service
public class ItemsTypeServiceImpl implements ItemsTypeService {
    private ItemsTypeMapper itemsTypeMapper;



    @Autowired
    public void setItemsTypeMapper(ItemsTypeMapper itemsTypeMapper) {
        this.itemsTypeMapper = itemsTypeMapper;
    }

    @Override
    public List<ItemsType> selItemsType() {
        return itemsTypeMapper.selItemsType();
    }

    @Override
    public List<ItemsType> selItemsTypeAndCounts() {
        return itemsTypeMapper.selItemsTypeAndCounts();
    }
}
