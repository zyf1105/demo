package com.buba.reportForm.service;

import com.buba.reportForm.pojo.ItemsType;
import com.buba.system.pojo.Job;
import com.buba.system.pojo.Menu;

import java.util.List;

/**
 * 货物种类业务层
 */
public interface ItemsTypeService {
    List<ItemsType> selItemsType();

    List<ItemsType> selItemsTypeAndCounts();
}
