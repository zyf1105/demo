package com.buba.reportForm.dao;

import com.buba.reportForm.pojo.ItemsType;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 货物种类持久层
 */
@Repository
@Mapper
public interface ItemsTypeMapper {

    List<ItemsType> selItemsType();

    List<ItemsType> selItemsTypeAndCounts();
}
