package com.buba.customer.dao;

import com.buba.customer.pojo.Customer;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * 客户持久层接口
 */
@Repository
@Mapper
public interface CustomerMapper {
    List<Customer> selCustomer(Map<String, Object> map);

    long selCustomerCount(Map<String, Object> map);

    long addCustomer(Customer customer);

    long delCustomer(List<String> list);

    long updCustomer(Customer customer);
}
