package com.buba.customer.pojo;

import lombok.Data;

/**
 * 客户实体类
 */
@Data
public class Customer {
    private Integer customerNo;
    private String customerDriverNo;
    private String companyName;
    private String companyAddress;
    private Integer companyCredit;
    private String phone;
}
