package com.buba.customer.controller;

import com.buba.customer.pojo.Customer;
import com.buba.customer.pojo.Page;
import com.buba.customer.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 客户控制层
 */
@RestController
@RequestMapping("/CustomerController")
public class CustomerController {
    private CustomerService customerService;


    /**
     * 修改客户信息
     * @param customer
     * @return
     */
    @RequestMapping("/updCustomer")
    public Map<String,Object> updCustomer(Customer customer){
        Map<String,Object> map = new HashMap<>();
        System.out.println(customer);
        long count = customerService.updCustomer(customer);
        if (count!=0){
            map.put("msg","修改成功！");
        }else{
            map.put("msg","修改失败！");
        }
        return map;
    }


    /**
     * 删除用户信息
     * @param cusIds
     * @return
     */
    @RequestMapping("/delCustomer")
    public Map<String,Object> delCustomer(String cusIds){
        Map<String,Object> map = new HashMap<>();
        System.out.println(cusIds);
        String[] cusId =  cusIds.split(",");
        List<String> list = Arrays.asList(cusId);
        long count = customerService.delCustomer(list);
        if (count!=0){
            map.put("msg","删除成功！");
        }else{
            map.put("msg","删除失败！");
        }
        return map;
    }



    /**
     * 添加客户信息
     * @param customer
     * @return
     */
    @RequestMapping("/addCustomer")
    public Map<String,Object> addCustomer(Customer customer){
        Map<String,Object> map = new HashMap<>();
        String id = System.currentTimeMillis()+"";
        customer.setCustomerDriverNo(id);
        long count = customerService.addCustomer(customer);
        if (count!=0){
            map.put("msg","添加成功！");
        }else{
            map.put("msg","添加失败！");
        }
        return map;
    }



    /**
     * 查询客户信息
     * @param map
     * @return
     */
    @RequestMapping("/selCustomer")
    public Page<Customer> selCustomer(@RequestParam Map<String,Object> map){
        Page<Customer> page = new Page<>(Integer.parseInt(map.get("pageNow").toString()));
        map.put("page",page);
        //查询客户信息
        List<Customer> customerList = customerService.selCustomer(map);
        //查询总条数
        long count = customerService.selCustomerCount(map);

        page.computeTotal(count);
        page.setCustomerList(customerList);
        return page;
    }

    @Autowired
    public void setCustomerService(CustomerService customerService) {
        this.customerService = customerService;
    }
}
