package com.buba.customer.service.impl;

import com.buba.customer.dao.CustomerMapper;
import com.buba.customer.pojo.Customer;
import com.buba.customer.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * 客户业务层实现类
 */
@Service
public class CustomerServiceImpl implements CustomerService {
    private CustomerMapper customerMapper;
    @Override
    public List<Customer> selCustomer(Map<String, Object> map) {
        return customerMapper.selCustomer(map);
    }

    @Override
    public long selCustomerCount(Map<String, Object> map) {
        return customerMapper.selCustomerCount(map);
    }

    @Override
    public long addCustomer(Customer customer) {
        return customerMapper.addCustomer(customer);
    }

    @Override
    public long delCustomer(List<String> list) {
        return customerMapper.delCustomer(list);
    }

    @Override
    public long updCustomer(Customer customer) {
        return customerMapper.updCustomer(customer);
    }

    @Autowired
    public void setCustomerMapper(CustomerMapper customerMapper) {
        this.customerMapper = customerMapper;
    }
}
