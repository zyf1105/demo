package com.buba.customer.service;

import com.buba.customer.pojo.Customer;

import java.util.List;
import java.util.Map;

public interface CustomerService {
    List<Customer> selCustomer(Map<String, Object> map);

    long selCustomerCount(Map<String, Object> map);

    long addCustomer(Customer customer);

    long delCustomer(List<String> list);

    long updCustomer(Customer customer);
}
