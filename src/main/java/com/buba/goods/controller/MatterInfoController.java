package com.buba.goods.controller;

import com.buba.customer.pojo.Customer;
import com.buba.goods.pojo.MatterInfo;
import com.buba.goods.service.MatterInfoService;
import com.buba.util.Excel;
import com.buba.util.GenerateKey;
import com.buba.util.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 货物明细表控制层
 */
@RestController
@RequestMapping("/MatterInfoController")
public class MatterInfoController {
    private MatterInfoService matterInfoService;


    @RequestMapping("/selMatterInfoItemsInfoNo")
    public Map<String, Object> selCustomerNo() {
        Map<String, Object> map = new HashMap<>();
        List<MatterInfo> list = matterInfoService.selMatterInfoAll(map);
        map.put("list", list);
        return map;
    }

    /**
     * 导入excel方法
     * @param file
     * @return
     */
    @RequestMapping("/importExcel")
    public Map<Object,String> importExcel(@RequestParam("file") MultipartFile file)  {
        System.out.println("进入后台"+file.getOriginalFilename());
        //实例化工具类
        Excel<MatterInfo> excel = new Excel<>();
        //调用导入方法
        List<MatterInfo> list = excel.importExcel(file, MatterInfo.class);
        System.out.println(list);
        Long count = matterInfoService.addMatterInfoByList(list);
        Map<Object,String> map = new HashMap<>();
        if (count!=0){
            map.put("msg","导入成功");
        }else {
            map.put("msg","导入失败");
        }
        return map;
    }






    /**
     * 导出excel方法
     * @param response
     * @throws Exception
     */
    @RequestMapping("/exportCustomerExcel")
    public void exportCustomerExcel(HttpServletResponse response,String itemsName,String itemsProduction) throws Exception {
        System.out.println("进入后台页面");
        Map<String,Object> map = new HashMap<>();
        map.put("itemsName",itemsName);
        map.put("itemsProduction",itemsProduction);
        System.out.println(map);
        //查询客户信息
        List<MatterInfo> matterInfo = matterInfoService.selMatterInfoByCondition(map);
        System.out.println(matterInfo);
        Excel<MatterInfo> excel = new Excel<>();
        excel.exportExcel("货物明细表",matterInfo,MatterInfo.class,response);

    }


    /**
     * 修改货物明细表信息
     * @param matterInfo
     * @return
     */
    @RequestMapping("/updMatterInfo")
    public Map<String,Object> updMatterInfo(MatterInfo matterInfo){


        Map<String,Object> map = new HashMap<>();
        System.out.println(matterInfo);
        long count = matterInfoService.updMatterInfo(matterInfo);
        if (count!=0){
            map.put("msg","修改成功！");
        }else{
            map.put("msg","修改失败！");
        }
        return map;
    }


    /**
     * 删除货物明细表信息
     * @param mIds
     * @return
     */
    @RequestMapping("/delMatterInfo")
    public Map<String,Object> delMatterInfo(String mIds){
        Map<String,Object> map = new HashMap<>();
        String[] mpId =  mIds.split(",");
        List<String> list = Arrays.asList(mpId);
        long count = matterInfoService.delMatterInfo(list);
        if (count!=0){
            map.put("msg","删除成功！");
        }else{
            map.put("msg","删除失败！");
        }
        return map;
    }



    /**
     * 添加货物明细表信息
     * @param matterInfo
     * @return
     */
    @RequestMapping("/addMatterInfo")
    public Map<String,Object> addMatterInfo(MatterInfo matterInfo){
        Map<String,Object> map = new HashMap<>();
        String ItemsInfoNo = GenerateKey.getKey("mi");
        matterInfo.setItemsInfoNo(ItemsInfoNo);
        System.out.println(matterInfo);
        long count = matterInfoService.addMatterInfo(matterInfo);
        if (count!=0){
            map.put("msg","添加成功！");
        }else{
            map.put("msg","添加失败！");
        }
        return map;
    }



    /**
     * 查询货物明细表信息
     * @param map
     * @return
     */
    @RequestMapping("/selMatterInfo")
    public Page<MatterInfo> selMatterInfo(@RequestParam Map<String,Object> map){
        Page<MatterInfo> page = new Page<>(Integer.parseInt(map.get("pageNow").toString()));
        map.put("page",page);
        //查询客户信息
        List<MatterInfo> matterInfo = matterInfoService.selMatterInfo(map);
        //查询总条数
        long count = matterInfoService.selMatterInfoCount(map);

        page.computeTotal(count);
        page.setList(matterInfo);
        return page;
    }


    @Autowired
    public void setMatterInfoService(MatterInfoService matterInfoService) {
        this.matterInfoService = matterInfoService;
    }
}
