package com.buba.goods.controller;

import com.buba.goods.pojo.MatterInfo;
import com.buba.goods.pojo.MatterPrice;
import com.buba.goods.service.MatterPriceService;
import com.buba.util.GenerateKey;
import com.buba.util.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 物质价格表控制层
 */
@RestController
@RequestMapping("/MatterPriceController")
public class MatterPriceController {
    private MatterPriceService matterPriceService;


    @RequestMapping("/selMatterPriceItemsNo")
    public Map<String, Object> selMatterPriceItemsNo() {
        Map<String, Object> map = new HashMap<>();
        List<MatterPrice> list = matterPriceService.selMatterPriceAll(map);
        map.put("list", list);
        return map;
    }



    /**
     * 修改物质价格表信息
     * @param matterPrice
     * @return
     */
    @RequestMapping("/updMatterPrice")
    public Map<String,Object> updMatterPrice(MatterPrice matterPrice){
        Map<String,Object> map = new HashMap<>();
        System.out.println(matterPrice);
        long count = matterPriceService.updMatterPrice(matterPrice);
        if (count!=0){
            map.put("msg","修改成功！");
        }else{
            map.put("msg","修改失败！");
        }
        return map;
    }


    /**
     * 删除物质价格表信息
     * @param mpIds
     * @return
     */
    @RequestMapping("/delMatterPrice")
    public Map<String,Object> delMatterPrice(String mpIds){
        Map<String,Object> map = new HashMap<>();
        System.out.println(mpIds);
        String[] mpId =  mpIds.split(",");
        List<String> list = Arrays.asList(mpId);
        long count = matterPriceService.delMatterPrice(list);
        if (count!=0){
            map.put("msg","删除成功！");
        }else{
            map.put("msg","删除失败！");
        }
        return map;
    }



    /**
     * 添加物质价格表信息
     * @param matterPrice
     * @return
     */
    @RequestMapping("/addMatterPrice")
    public Map<String,Object> addMatterPrice(MatterPrice matterPrice){
        Map<String,Object> map = new HashMap<>();
        String ItemsNo = GenerateKey.getKey("mp");
        matterPrice.setItemsNo(ItemsNo);
        System.out.println(matterPrice);
        long count = matterPriceService.addMatterPrice(matterPrice);
        if (count!=0){
            map.put("msg","添加成功！");
        }else{
            map.put("msg","添加失败！");
        }
        return map;
    }



    /**
     * 查询物质价格表信息
     * @param map
     * @return
     */
    @RequestMapping("/selMatterPrice")
    public Page<MatterPrice> selMatterPrice(@RequestParam Map<String,Object> map){
        Page<MatterPrice> page = new Page<>(Integer.parseInt(map.get("pageNow").toString()));
        map.put("page",page);
        //查询客户信息
        List<MatterPrice> matterPriceList = matterPriceService.selMatterPrice(map);
        //查询总条数
        long count = matterPriceService.selMatterPriceCount(map);

        page.computeTotal(count);
        page.setList(matterPriceList);
        return page;
    }


    @Autowired
    public void setMatterPriceService(MatterPriceService matterPriceService) {
        this.matterPriceService = matterPriceService;
    }
}
