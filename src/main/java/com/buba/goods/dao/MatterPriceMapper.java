package com.buba.goods.dao;

import com.buba.goods.pojo.MatterPrice;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * 物质价格表持久层接口
 */
@Mapper
@Repository
public interface MatterPriceMapper {
    long updMatterPrice(MatterPrice matterPrice);

    long delMatterPrice(List<String> list);

    long addMatterPrice(MatterPrice matterPrice);

    List<MatterPrice> selMatterPrice(Map<String, Object> map);

    long selMatterPriceCount(Map<String, Object> map);

    Integer selMatterPriceItemsNo(Map<String, Object> map);

    List<MatterPrice> selMatterPriceAll(Map<String, Object> map);
}
