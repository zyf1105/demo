package com.buba.goods.dao;

import com.buba.goods.pojo.MatterInfo;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * 货物明细表持久层接口
 */
@Mapper
@Repository
public interface MatterInfoMapper {
    long updMatterInfo(MatterInfo matterInfo);

    long delMatterInfo(List<String> list);

    long addMatterInfo(MatterInfo matterInfo);

    List<MatterInfo> selMatterInfo(Map<String, Object> map);

    long selMatterInfoCount(Map<String, Object> map);

    List<MatterInfo> selMatterInfoByCondition(Map<String, Object> map);

    Long addMatterInfoByList(List<MatterInfo> list);

    List<MatterInfo> selMatterInfoAll(Map<String, Object> map);
}
