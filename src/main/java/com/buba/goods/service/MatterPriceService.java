package com.buba.goods.service;

import com.buba.goods.pojo.MatterPrice;

import java.util.List;
import java.util.Map;

/**
 * 物质价格表业务层接口
 */
public interface MatterPriceService {
    long updMatterPrice(MatterPrice matterPrice);

    long delMatterPrice(List<String> list);

    long addMatterPrice(MatterPrice matterPrice);

    List<MatterPrice> selMatterPrice(Map<String, Object> map);

    long selMatterPriceCount(Map<String, Object> map);

    Integer selMatterPriceItemsNo(Map<String, Object> map);

    List<MatterPrice> selMatterPriceAll(Map<String, Object> map);
}
