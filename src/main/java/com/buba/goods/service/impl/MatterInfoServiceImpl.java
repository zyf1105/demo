package com.buba.goods.service.impl;

import com.buba.goods.dao.MatterInfoMapper;
import com.buba.goods.pojo.MatterInfo;
import com.buba.goods.service.MatterInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * 货物明细表业务层实现类
 */
@Service
public class MatterInfoServiceImpl implements MatterInfoService {
    private MatterInfoMapper matterInfoMapper;

    @Autowired
    public void setMatterInfoMapper(MatterInfoMapper matterInfoMapper) {
        this.matterInfoMapper = matterInfoMapper;
    }

    @Override
    public long updMatterInfo(MatterInfo matterInfo) {
        return matterInfoMapper.updMatterInfo(matterInfo);
    }

    @Override
    public long delMatterInfo(List<String> list) {
        return matterInfoMapper.delMatterInfo(list);
    }

    @Override
    public long addMatterInfo(MatterInfo matterInfo) {
        return matterInfoMapper.addMatterInfo(matterInfo);
    }

    @Override
    public List<MatterInfo> selMatterInfo(Map<String, Object> map) {
        return matterInfoMapper.selMatterInfo(map);
    }

    @Override
    public long selMatterInfoCount(Map<String, Object> map) {
        return matterInfoMapper.selMatterInfoCount(map);
    }

    @Override
    public List<MatterInfo> selMatterInfoByCondition(Map<String, Object> map) {
        return matterInfoMapper.selMatterInfoByCondition(map);
    }

    @Override
    public Long addMatterInfoByList(List<MatterInfo> list) {
        return matterInfoMapper.addMatterInfoByList(list);
    }

    @Override
    public List<MatterInfo> selMatterInfoAll(Map<String, Object> map) {
        return matterInfoMapper.selMatterInfoAll(map);
    }
}
