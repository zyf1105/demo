package com.buba.goods.service.impl;

import com.buba.goods.dao.MatterPriceMapper;
import com.buba.goods.pojo.MatterPrice;
import com.buba.goods.service.MatterPriceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * 物质价格表业务层实现类
 */
@Service
public class MatterPriceServiceImpl implements MatterPriceService {

    private MatterPriceMapper matterPriceMapper;

    @Autowired
    public void setMatterPriceMapper(MatterPriceMapper matterPriceMapper) {
        this.matterPriceMapper = matterPriceMapper;
    }

    @Override
    public long updMatterPrice(MatterPrice matterPrice) {
        return matterPriceMapper.updMatterPrice( matterPrice);
    }

    @Override
    public long delMatterPrice(List<String> list) {
         return matterPriceMapper.delMatterPrice( list);
    }

    @Override
    public long addMatterPrice(MatterPrice matterPrice) {
         return matterPriceMapper.addMatterPrice( matterPrice);
    }

    @Override
    public List<MatterPrice> selMatterPrice(Map<String, Object> map) {
         return matterPriceMapper.selMatterPrice( map);
    }

    @Override
    public long selMatterPriceCount(Map<String, Object> map) {
         return matterPriceMapper.selMatterPriceCount( map);
    }

    @Override
    public Integer selMatterPriceItemsNo(Map<String, Object> map) {
        return matterPriceMapper.selMatterPriceItemsNo( map);
    }

    @Override
    public List<MatterPrice> selMatterPriceAll(Map<String, Object> map) {
        return matterPriceMapper.selMatterPriceAll( map);
    }
}
