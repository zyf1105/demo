package com.buba.goods.service;

import com.buba.goods.pojo.MatterInfo;

import java.util.List;
import java.util.Map;

public interface MatterInfoService {
    long updMatterInfo(MatterInfo matterInfo);

    long delMatterInfo(List<String> list);

    long addMatterInfo(MatterInfo matterInfo);

    List<MatterInfo> selMatterInfo(Map<String, Object> map);

    long selMatterInfoCount(Map<String, Object> map);

    List<MatterInfo> selMatterInfoByCondition(Map<String, Object> map);

    Long addMatterInfoByList(List<MatterInfo> list);

    List<MatterInfo> selMatterInfoAll(Map<String, Object> map);
}
