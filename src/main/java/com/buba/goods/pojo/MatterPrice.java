package com.buba.goods.pojo;

import lombok.Data;

import java.math.BigDecimal;

/**
 * 物质价格表
 */
@Data
public class MatterPrice {
    private String itemsNo;
    private String itemsName;
    private String itemsUnit;
    private BigDecimal itemsTax;
    private BigDecimal itemsCost;
    private BigDecimal itemsAddTranCost;
    private BigDecimal itemsStoreCounts;

}
