package com.buba.goods.pojo;

import com.buba.goods.annotation.ExcelCell;
import lombok.Data;

/**
 * 货物明细表
 */
@Data
public class MatterInfo {
    @ExcelCell(value="货物编号",type = "")
    private String itemsInfoNo;
    @ExcelCell(value="货物名称",type = "")
    private String itemsName;
    @ExcelCell(value="货物产地",type = "")
    private String itemsProduction;
    @ExcelCell(value="货物规格",type = "")
    private String itemsScale;
    @ExcelCell(value="货物可供数量",type = "Integer")
    private Integer itemsCounts;
    @ExcelCell(value="货物采购合同号",type = "")
    private String itemsOrderNo;
    @ExcelCell(value="货物加工合同号",type = "")
    private String itemsAddNo;
    @ExcelCell(value="货物运输合同号",type = "")
    private String itemsTtranNo;
    @ExcelCell(value="货物入库合同号",type = "")
    private String itemsInStoreNo;
}
